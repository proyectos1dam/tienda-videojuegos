package botigavideojocs;

import java.util.Scanner;
import java.util.Arrays;

public class BotigaVideojocs {
    
    private static Scanner teclado;
    final static int CODMINJUEGO = 1;
    final static int CODMAXJUEGO = 10;
    
    public static void main(String[] args) {
        teclado = new Scanner(System.in);
        empezar();
        
    }
    
    public static void mostrarMenu() {
        System.out.print("Bienvenido a la tienda de videojuegos. Elija una opción:");
        System.out.print("\n    1. Consultar catálogo de videojuegos\n    2. Consultar stock de videojuegos\n"
                + "    3. Mostrar catálogo ordenado por stock\n    4. Mostrar catálogo ordenado por precio\n"
                + "    5. Registrar venta de videojuegos\n    6. Registrar devolución de videojuegos\n    7. Salir de la aplicación\n\n");
    }
    
    public static int seleccionarOpcion(String preg) {
        boolean correcte = false;
        int num = 0;
        while (correcte == false) {
            System.out.print(preg);
            if (teclado.hasNextInt()) {
                num = teclado.nextInt();
                if (num > 0 && num < 8) {
                    correcte = true;
                } else {
                    System.out.println("Opción incorrecta. Introduce un numero entre 1-7");
                }
                
            } else {
                System.out.println("Opción incorrecta. Introduce un numero entre 1-7");
                teclado.next();
            }
        }
        return num;
    }
    
    public static void empezar() {
        mostrarMenu();
        String[][] catalogo = crearBD();
        int[][] compra = new int[0][0];
        int codCompra = 0;
        boolean sal = false;
        while (sal == false) {
            int opcion = seleccionarOpcion("\nIntroduce opción: ");
            switch (opcion) {
                case 1:
                    String[][] catalogo1 = new String[catalogo.length][catalogo.length];
                    catalogo1 = copiarArray(catalogo);
                    consCatalogo(catalogo1);
                    break;
                case 2:
                    String[][] catalogo2 = copiarArray(catalogo);
                    consStock(catalogo2);
                    break;
                case 3:
                    String[][] ordXStock = copiarArray(catalogo);
                    ordXStock = ordenarPorStock(ordXStock);
                    ordxStock(ordXStock);
                    break;
                case 4:
                    String[][] ordXPrecio = copiarArray(catalogo);
                    ordXPrecio = ordenarPorPrecio(ordXPrecio);
                    ordxPrecio(ordXPrecio);
                    break;
                case 5:
                    String[][] catalogoCopia = Arrays.copyOf(catalogo, catalogo.length);
                    codCompra++;
                    compra = registrarVenta(compra, catalogoCopia, codCompra);
                    break;
                case 6:
                    int[][] compraCopia = Arrays.copyOf(compra, compra.length);
                    registrarDevolucion(catalogo, compraCopia);
                    break;
                case 7: {
                    salir();
                    sal = true;
                    break;
                }
            }
        }
    }
    
    public static void mostrarArray(String[][] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println();
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(" " + array[i][j]);
            }
        }
    }
    
    public static void consCatalogo(String[][] array) {
        System.out.println("\nCatálogo de videojuegos: \n");
        System.out.print("+--------+--------------------+-------+\n");
        System.out.print("| Código | Nombre             | Precio|\n");
        System.out.print("+--------+--------------------+-------+");
        for (int i = 0; i < array.length; i++) {
            System.out.printf("\n| %7s|%19s |%6s |  ", array[i][0], array[i][1], array[i][2]);
        }
        System.out.print("\n+--------+--------------------+-------+");
    }
    
    public static void consStock(String[][] array) {
        System.out.println("\nStock de videojuegos: \n");
        System.out.print("+--------+--------------------+-------+\n");
        System.out.print("| Código | Nombre             | Stock |\n");
        System.out.print("+--------+--------------------+-------+");
        for (int i = 0; i < array.length; i++) {
            System.out.print("\n");
            System.out.printf("|%7s |%19s |%6s |  ", array[i][0], array[i][1], array[i][3]);
            
        }
        System.out.print("\n+--------+--------------------+-------+\n");
    }
    
    public static void ordxStock(String[][] array) {
        System.out.print("\nStock de videojuegos: ");
        System.out.print("\n+--------+--------------------+-------+\n| Código | Nombre             | Stock |\n+--------+--------------------+-------+");
        for (int i = 0; i < array.length; i++) {
            System.out.print("\n");
            System.out.printf("|%7s |%19s |%6s |  ", array[i][0], array[i][1], array[i][3]);
        }
        System.out.print("\n+--------+--------------------+-------+\n");
    }
    
    public static void ordxPrecio(String[][] array) {
        
        System.out.print("\nStock de videojuegos: ");
        System.out.print("\n+--------+--------------------+-------+\n| Código | Nombre             | Stock |\n+--------+--------------------+-------+");
        for (int i = 0; i < array.length; i++) {
            System.out.print("\n");
            System.out.printf("|%7s |%19s |%6s |  ", array[i][0], array[i][1], array[i][2]);
        }
        System.out.print("\n+--------+--------------------+-------+\n");
    }
    
    public static int[][] registrarVenta(int[][] arrayCompra, String[][] arrayCopia, int codCompra) {
        
        boolean noComprarMas = false;
        float totalAPagar = 0;
        int codJuego = 0;
        int cant = 0;
        boolean esNuevaCompra = true;
        String continuar = "";
        
        while (noComprarMas == false) {
            System.out.print("\nIntroduzca el código del videojuego que desea comprar: ");
            if (teclado.hasNextInt()) {
                codJuego = teclado.nextInt();
                if (codJuego >= CODMINJUEGO && codJuego <= CODMAXJUEGO) {
                    System.out.print("\nIntroduzca la cantidad de unidades que desea comprar: ");
                    if (teclado.hasNextInt()) {
                        cant = teclado.nextInt();
                        if (cant <= Integer.valueOf(arrayCopia[codJuego - 1][3]) && cant != 0) {
                            arrayCompra = aumentarArray(arrayCompra);
                            arrayCompra[arrayCompra.length - 1][0] = codCompra;
                            arrayCompra[arrayCompra.length - 1][1] = Integer.valueOf(arrayCopia[codJuego - 1][0]);
                            arrayCompra[arrayCompra.length - 1][2] = cant;
                            
                            float precio = Float.valueOf(arrayCopia[codJuego - 1][2]);
                            float total = precio * cant;
                            totalAPagar = totalAPagar + total;
                            
                            int temp = Integer.valueOf(arrayCopia[codJuego - 1][3]);
                            int renovaStock = temp - cant;
                            arrayCopia[codJuego - 1][3] = String.valueOf(renovaStock);
                            
                            System.out.print("¿Desea comprar algún videojuego más? (S/N): ");
                            continuar = teclado.next();
                            
                            if (continuar.equals("N") || continuar.equals("n")) {
                                mostrarTotalVenta(arrayCopia, arrayCompra, codCompra);
                                System.out.printf("Total a pagar: %.2f ?\n\nGracias por su compra.\n\n", totalAPagar);
                                noComprarMas = true;
                                break;
                            } else if (continuar.equals("S") || continuar.equals("s")) {
                                continue;
                            } else {
                                System.out.print("\nOpción incorrecta.");
                                break;
                            }
                        } else {
                            System.out.print("\nLo siento, no tenemos este videojuego disponible o no tenemos tantas unidades disponibles.\n");
                        }
                    }
                } else {
                    System.out.print("\nEl código del juego no es correcto.\n");
                }
            } else {
                System.out.println("\nOpción incorrecta. Introduce un número válido.\n");
            }
        }
        return arrayCompra;
    }
    
    public static void registrarDevolucion(String[][] arrayCat, int[][] arrayCompras) {
        boolean novenderMas = false;
        int codVenta = 0;
        int cant = 0;
        String continuar = "";
        while (novenderMas == false) {
            System.out.print("\nIntroduzca el código de venta: ");
            if (teclado.hasNextInt()) {
                codVenta = teclado.nextInt();
                arrayCompras = decrecerArray(arrayCompras, codVenta);
                System.out.print("\nProcesando devolución...\n");
                System.out.printf("\nSu devolución ha sido realizada. Se devuelve un importe de .2f €");
                System.out.print("¿Desea intentar otra devolución? (S/N)?: ");
                continuar = teclado.next();
                if (continuar.equals("N") || continuar.equals("n")) {
                    novenderMas = true;
                    break;
                } else if (continuar.equals("S") || continuar.equals("s")) {
                }
                break;
            } else {
                System.out.println("No podemos localizar esta venta.");
                break;
            }
        }
    }
    
    public static void salir() {
        System.out.println("\nAdiós\n");
    }
    
    public static String meterStock() {
        int stock = generarNumAleatorio(0, 20);
        return String.valueOf(stock);        
    }
    
    public static String[][] crearBD() {
        String[][] catalogo = {{"1", "Super Mario Bros", "19.99", meterStock()}, {"2", "The legend of Zelda", "24.99", meterStock()}, {"3", "Sonic the hedgehog", "14.99", meterStock()},
        {"4", "Tetris", "9.99", meterStock()}, {"5", "Pac-Man", "4.99", meterStock()}, {"6", "Street Fighter II", "29.99", meterStock()}, {"7", "Doom", "39.99", meterStock()}, {"8", "Minecraft", "19.99", meterStock()},
        {"9", "The Sims", "34.99", meterStock()}, {"10", "Grand Theft Auto V", "49.99", meterStock()}};
        return catalogo;
    }
    
    public static String[][] copiarArray(String[][] array) {
        String[][] copia = new String[array.length][array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                copia[i][j] = array[i][j];
            }
        }
        return copia;
    }
    
    public static int[][] copiarArrayInts(int[][] array) {
        int[][] copia = new int[array.length][array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                copia[i][j] = array[i][j];
            }
        }
        return copia;
    }
    
    public static int[][] decrecerArray(int[][] arrayCompras, int codVenta) {
        int numeroVentas = contarVentas(arrayCompras, codVenta);
        int nuevoTamaño = arrayCompras.length - numeroVentas;
    int[][] copia = new int[nuevoTamaño][3];
    int ind = 0;
    for (int i = 0; i < arrayCompras.length; i++) {
        if (arrayCompras[i][0] != codVenta) {
            copia[ind] = arrayCompras[i];
            ind++;
        }
    }
    return copia;
}
    
    public static int contarVentas(int[][] arrayCompras, int codVenta) {
        int cont = 0;
        for (int i = 0; i < arrayCompras.length; i++) {
            if (arrayCompras[i][0] == codVenta) {
                cont++;
            }
        }
        return cont;
    }
    
    public static int generarNumAleatorio(int min, int max) {
        return (int) (int) (Math.random() * (max - min + 1)) + min;
    }
    
    public static int[][] aumentarArray(int[][] array) {
        array = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = new int[3];
        return array;
    }
    
    private static String[][] ordenarPorStock(String[][] v) {
        String aux;
        for (int i = 0; i < v.length - 1; i++) {
            for (int k = i + 1; k < v.length; k++) {
                if (Double.valueOf(v[i][3]) > Double.valueOf(v[k][3])) {
                    // intercanviar valors
                    aux = v[i][3];
                    v[i][3] = v[k][3];
                    v[k][3] = aux;
                    
                    aux = v[i][2];
                    v[i][2] = v[k][2];
                    v[k][2] = aux;
                    
                    aux = v[i][0];
                    v[i][0] = v[k][0];
                    v[k][0] = aux;
                    
                    aux = v[i][1];
                    v[i][1] = v[k][1];
                    v[k][1] = aux;
                }
            }
        }
        return v;
    }
    
    private static String[][] ordenarPorPrecio(String[][] v) {
        String aux;
        for (int i = 0; i < v.length - 1; i++) {
            for (int k = i + 1; k < v.length; k++) {
                if (Double.valueOf(v[i][2]) < Double.valueOf(v[k][2])) {
                    // intercanviar valors
                    aux = v[i][2];
                    v[i][2] = v[k][2];
                    v[k][2] = aux;
                    
                    aux = v[i][0];
                    v[i][0] = v[k][0];
                    v[k][0] = aux;
                    
                    aux = v[i][1];
                    v[i][1] = v[k][1];
                    v[k][1] = aux;
                    
                    aux = v[i][3];
                    v[i][3] = v[k][3];
                    v[k][3] = aux;
                }                
            }
        }
        return v;
    }
    
    private static void mostrarTotalVenta(String[][] arrayOriginal, int[][] arrayVenta, int cod) {
        System.out.printf("\nResumen de la venta (código %s): ", cod);
        System.out.print("\n+--------------+-----------------+----------+\n| Código Venta | Código Juego    | Unidades |\n+--------------+-----------------+----------+");
        for (int i = 0; i < arrayVenta.length; i++) {
            if (Integer.valueOf(arrayVenta[i][0]) == cod) {
                System.out.print("\n");
                System.out.printf("|%13s |%16s |%9s |", arrayVenta[i][0], arrayVenta[i][1], arrayVenta[i][2]);
            }
        }
        System.out.print("\n+--------------+-----------------+----------+\n");
        
    }
    
}
